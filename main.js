const fs = require('fs');
const WebSocketClient = require('websocket').client;
const express = require('express');
const app = express();
const client = new WebSocketClient();

const MARKETS = ['PLCUX_PLCU', 'PLCU_USDT']
const COMPUTED_MARKETS = [{ market: 'PLCUX_USDT', calcMethod: () => currentPrices.get('PLCUX_PLCU') * currentPrices.get('PLCU_USDT') }]
const currentPrices = new Map();
const pricesHistory = new Map();

function flush() {
  Array.from(pricesHistory.keys()).forEach(market => {
    fs.writeFile(market + '.json', JSON.stringify(pricesHistory.get(market)), (err) => {
      if (err) return console.log(err);
    })
  })

  console.log('flushed');
}

function loadDataFromFiles() {
  const allMarkets = COMPUTED_MARKETS.map(market => market.market).concat(MARKETS);
  allMarkets.forEach(market => {
    fs.readFile(market + '.json', (err, data) => {
      if (err) console.log(err);
      else pricesHistory.set(market, JSON.parse(data))
    });
  })
}

function updatePrice(market, price) {
  if (!pricesHistory.has(market)) {
    pricesHistory.set(market, [])
  }

  currentPrices.set(market, price)
  pricesHistory.get(market).push({ time: new Date().getTime(), price: price })
}

loadDataFromFiles();

client.on('connectFailed', function (error) {
  console.log('Connect Error: ' + error.toString());
});

client.on('connect', function (connection) {
  console.log('Connection established!');
  const marketMessage = { 'method': 'market.subscribe', 'params': [], 'id': 10 }
  connection.send(JSON.stringify(marketMessage))

  connection.on('error', function (error) {
    console.log('Connection error: ' + error.toString());
  });

  connection.on('close', function () {
    console.log('Connection closed!');
    client.connect('wss://ws.coinsbit.io/');
  });

  connection.on('message', function (message) {
    data = JSON.parse(message.utf8Data);

    if (data.result) {
      console.log('subscribed to markets');
    } else if (data.method === 'market.update') {
      const markets = data.params.filter(param => MARKETS.includes(param.market))
      markets.forEach(market => {
        updatePrice(market.market, parseFloat(market.last))
      })

      COMPUTED_MARKETS.forEach(market => {
        updatePrice(market.market, market.calcMethod())
      })

      flush()
    }
  });
});

app.use(express.static('static'))

app.get('/market/:market', (req, res) => {
  res.json({ market: req.params.market, price: currentPrices.get(req.params.market) });
});

app.get('/market/:market/history', (req, res) => {
  const count = req.query.count ? req.query.count : 100
  res.json(pricesHistory.get(req.params.market).slice(-count));
});

app.get('/markets', (req, res) => {
  res.json(Array.from(pricesHistory.keys()));
});

client.connect('wss://ws.coinsbit.io/');

app.listen(3000, () => {
  console.log('Server running on port 3000');
});